import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class foodmachineGUI {
    private JPanel root;
    private JButton food1;
    private JButton food2;
    private JButton food3;
    private JButton food4;
    private JButton food5;
    private JButton food6;
    private JTextPane orderList;
    private JLabel TopLabel;
    private JLabel TopLabel2;
    private JButton checkOutButton;
    private JLabel Total;
    private JLabel Value;
    private JLabel Yen;
    private JOptionPane showMessageDialog;

    int value = 0;
    void order(String food) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering " +
                    food + " ! It will be served as soon as possible.");
            value += 100;
            Value.setText(Integer.toString(value));
            String currentText = orderList.getText();
            if(value == 100){
                orderList.setText(currentText + food);
            }
            else{
                orderList.setText(currentText + "\n" + food);
            }
        }
    }

    public foodmachineGUI() {
        food1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Tempura"); }
        });
        food2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Karaage"); }
        });
        food3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Gyoza"); }
        });
        food4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Udon"); }
        });
        food5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Yakisoba"); }
        });
        food6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Ramen"); }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout ?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " +
                            value + " yen.");
                    value = 0;
                    Value.setText("0");
                    orderList.setText("");
                }
            }
        });
    }

        public static void main(String[] args) {
            JFrame frame = new JFrame("foodmachineGUI");
            frame.setContentPane(new foodmachineGUI().root);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
        }
}

